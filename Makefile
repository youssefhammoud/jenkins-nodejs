all: install testcoverage
run:
		@npm start
install:
		@npm install
testcoverage:
		if [ -d "nyc-demo" ]; then rm -Rf nyc-demo; fi
		@mkdir nyc-demo
		@npm run test
		@npm run coverage
nothing:
